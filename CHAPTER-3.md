# Chapter 3: Advanced

This chapter contains some more complex concepts for the fast learner. 

## Assignment 1: Dockerize the backend

These days you hear and see Docker everywhere, because it is really great to use.
You should know how to build them in your pipeline. 

Gitlab uses Docker images (on the shared runners at least) to build your Software. 
This presents us with a problem; We are going to build Docker images from inside a Docker image!

Smart people already solved this problem, and runners exist which can run Docker in Docker, so we can build our images. We could also run them if we wanted to
While using the shared runners there are some things we need to configure:

- Configure the job to use the `docker:dind` service (dind = docker in docker)
- Configure the job with the docker tag, so it runs on a docker runner
- Configure the job to use the `docker:stable` image

All combined, it should look like this:

```yml
docker_job:
  image: docker:stable
  services:
  - docker:dind
  stage: ...
  script:
    - ...
  tags:
    - docker
```

Since we are using Docker in Docker, we need to instruct the docker in our container to connect to the docker daemon provided in the service.
We do this by defining environment variables:

```yml
variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
```

We also need a script to build our Docker image. Docker building generally consists of three steps:
- Building   - Building the image
- Tagging    - Giving the image a recognisable name
- Pushing    - Pushing the image to the Docker repository

Maybe you'll think; We don't have a Docker repository! We do have one though, Gitlab has Docker repositories built in!

You'll need the following script to make your build work:
 
```bash
docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
cd backend
mv target/*.jar target/app.jar
docker build . -t $CI_REGISTRY_IMAGE/backend:latest
docker push $CI_REGISTRY_IMAGE/backend:latest
```

Notice how it uses a lot of variables Gitlab has built in for our convenience? 
You can find a list of them here: https://docs.gitlab.com/ee/ci/variables/#predefined-variables-environment-variables

### Goal
Combine the things needed to build the docker image. Afterwards you should be able to see the image in your project's registry!

## Assignment 2: Supporting branch workflows
When working in a team, we often use branching to support our workflow. This might look like this:

- Branch master: Everything merged into here will be deployed to Acceptance and Production
- Branch develop: Everything merged into here will be deployed to Test
- Branch feature/*: Feature branches is where developers work, before merging it to develop. No deployment is done

As you can see, we would need multiple pipelines to support all!

Gitlab has ways to limit execution of jobs to certain branches. You can read all about it here:
https://docs.gitlab.com/ee/ci/yaml/#only-and-except-simplified

### Goal
Complete the following tasks:
- deployments are only done on the master and develop branch
- deployments of master and develop are done to different sub-buckets

## End of Chapter
You have successfully made it through the third chapter! You can compare your results with the `results/chapter-3` branch now!
